﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace POC_AzureFaceRecognition.Tests
{
    public class DetectFacesTests
    {
        public const string azureEndpoint = "https://utad-face-recognition.cognitiveservices.azure.com/";
        public const string azureSubscriptionKey = "ce292faec5be4b62a8ae089b817b08ad";

        public const string someFaceUrl = "https://csdx.blob.core.windows.net/resources/Face/Images/detection1.jpg";
        public const string someFaceUrl2 = "https://csdx.blob.core.windows.net/resources/Face/Images/detection5.jpg";
        public const string someFaceUrl3 = "https://csdx.blob.core.windows.net/resources/Face/Images/detection6.jpg";

        public readonly IEnumerable<string> someFaceUrls = new List<string>() { someFaceUrl, someFaceUrl2, someFaceUrl3 };

        public DetectFacesTests()
        {
            AzureFaceRecognitionClient.Initialize(azureEndpoint, azureSubscriptionKey);
        }

        [Fact]
        public async Task DetectFacesSingleUrl()
        {
            var res = await AzureFaceRecognitionClient.DetectFacesAsync(someFaceUrl);

            Assert.True(res.Success);
            Assert.NotNull(res.Value);
        }

        [Fact]
        public async Task DetectFacesMultiUrl()
        {
            var res = await AzureFaceRecognitionClient.DetectFacesAsync(someFaceUrls);

            Assert.True(res.Success);
            Assert.NotNull(res.Value);
            Assert.NotEmpty(res.Value);
        }
    }
}
