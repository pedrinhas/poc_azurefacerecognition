﻿using Microsoft.Azure.CognitiveServices.Vision.Face;
using Microsoft.Azure.CognitiveServices.Vision.Face.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POC_AzureFaceRecognition
{
    public static class AzureFaceRecognitionClient
    {
        private static IFaceClient _client;
        private static string _defaultRecognitionModel = RecognitionModel.Recognition01;
        private static IEnumerable<string> _validRecognitionModels = new List<string>() { RecognitionModel.Recognition01, RecognitionModel.Recognition02 };
        public static string Endpoint { get; private set; }
        public static bool IsInitialized => !(string.IsNullOrWhiteSpace(SubscriptionKey) || string.IsNullOrWhiteSpace(Endpoint));
        private static IFaceClient Client
        {
            get
            {
                ValidateIsInitialized();

                if (_client == null)
                {
                    _client = new FaceClient(new ApiKeyServiceClientCredentials(SubscriptionKey)) { Endpoint = Endpoint };
                }

                return _client;
            }
        }

        private static string SubscriptionKey { get; set; }
        public static async Task<AzureFaceValueResult<IEnumerable<DetectedFace>>> DetectFacesAsync(string url)
        {
            try
            {
                ValidateIsInitialized();
                ValidateStringParameter(url, nameof(url));

                var res = await InternalDetectFacesAsync(new List<string>() { url }, _defaultRecognitionModel);

                if (!res.Success)
                {
                    return new AzureFaceValueResult<IEnumerable<DetectedFace>>(res.Exception);
                }

                return new AzureFaceValueResult<IEnumerable<DetectedFace>>(res.Value[url]);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new AzureFaceValueResult<IEnumerable<DetectedFace>>(ex);
            }
        }

        public static async Task<AzureFaceValueResult<IEnumerable<DetectedFace>>> DetectFacesAsync(string url, string faceRecognitionModel)
        {
            try
            {
                ValidateIsInitialized();
                ValidateStringParameter(url, nameof(url));
                ValidateFaceRecognitionModelParameter(faceRecognitionModel, nameof(faceRecognitionModel));

                var res = await InternalDetectFacesAsync(new List<string>() { url }, faceRecognitionModel);

                if (!res.Success)
                {
                    return new AzureFaceValueResult<IEnumerable<DetectedFace>>(res.Exception);
                }

                return new AzureFaceValueResult<IEnumerable<DetectedFace>>(res.Value[url]);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new AzureFaceValueResult<IEnumerable<DetectedFace>>(ex);
            }
        }

        public static async Task<AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>> DetectFacesAsync(IEnumerable<string> urls)
        {
            try
            {
                ValidateIsInitialized();
                ValidateUrlListParameter(urls, nameof(urls));

                return await InternalDetectFacesAsync(urls, _defaultRecognitionModel);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>(ex);
            }
        }

        public static async Task<AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>> DetectFacesAsync(IEnumerable<string> urls, string faceRecognitionModel)
        {
            try
            {
                ValidateIsInitialized();
                ValidateUrlListParameter(urls, nameof(urls));
                ValidateFaceRecognitionModelParameter(faceRecognitionModel, nameof(faceRecognitionModel));

                return await InternalDetectFacesAsync(urls, faceRecognitionModel);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>(ex);
            }
        }

        public static AzureFaceResult Initialize(string endpoint, string subscriptionKey)
        {
            try
            {
                ValidateStringParameter(endpoint, nameof(endpoint));
                ValidateStringParameter(subscriptionKey, nameof(subscriptionKey));

                Endpoint = endpoint;
                SubscriptionKey = subscriptionKey;

                return new AzureFaceResult(IsInitialized);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new AzureFaceResult(ex);
            }
        }
        private static async Task<AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>> InternalDetectFacesAsync(IEnumerable<string> urls, string faceRecognitionModel)
        {
            try
            {
                var detectedFaces = new Dictionary<string, IEnumerable<DetectedFace>>();

                foreach (var url in urls)
                {
                    var mugs = await Client.Face.DetectWithUrlAsync(url, recognitionModel: faceRecognitionModel,
                returnFaceAttributes: new List<FaceAttributeType> { FaceAttributeType.Accessories, FaceAttributeType.Age,
                FaceAttributeType.Blur, FaceAttributeType.Emotion, FaceAttributeType.Exposure, FaceAttributeType.FacialHair,
                FaceAttributeType.Gender, FaceAttributeType.Glasses, FaceAttributeType.Hair, FaceAttributeType.HeadPose,
                FaceAttributeType.Makeup, FaceAttributeType.Noise, FaceAttributeType.Occlusion, FaceAttributeType.Smile });

                    detectedFaces.Add(url, mugs);
                }

                return new AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>(detectedFaces);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new AzureFaceValueResult<Dictionary<string, IEnumerable<DetectedFace>>>(ex);
            }
        }

        private static void ValidateFaceRecognitionModelParameter(string value, string fieldName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(fieldName);
            }
            if (!_validRecognitionModels.Contains(value))
            {
                throw new ArgumentException("Invalid face recognition model", fieldName);
            }
        }

        private static void ValidateIsInitialized()
        {
            if (!IsInitialized)
            {
                throw new InvalidOperationException($"The {nameof(AzureFaceRecognitionClient)} class was not initialized. Please call method {nameof(AzureFaceRecognitionClient.Initialize)} first");
            }
        }

        private static void ValidateStringParameter(string value, string fieldName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(fieldName);
            }
        }

        private static void ValidateUrlListParameter(IEnumerable<string> value, string fieldName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(fieldName);
            }
            if (!value.Any())
            {
                throw new ArgumentException("No URL values were found", fieldName);
            }
            if (value.All(x => string.IsNullOrWhiteSpace(x)))
            {
                throw new ArgumentException("No valid URL values were found", fieldName);
            }
        }
    }
}