﻿namespace POC_AzureFaceRecognition
{
    using System;

    /// <summary>
    /// Defines the <see cref="AzureFaceValueResult{T}" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AzureFaceValueResult<T> : AzureFaceResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AzureFaceValueResult{T}"/> class.
        /// </summary>
        /// <param name="value">The value<see cref="T"/></param>
        public AzureFaceValueResult(T value) : this(value != null, value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureFaceValueResult{T}"/> class.
        /// </summary>
        /// <param name="success">The success<see cref="bool"/></param>
        /// <param name="value">The value<see cref="T"/></param>
        public AzureFaceValueResult(bool success, T value) : base(success)
        {
            Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureFaceValueResult{T}"/> class.
        /// </summary>
        /// <param name="ex">The ex<see cref="Exception"/></param>
        public AzureFaceValueResult(Exception ex) : base(ex)
        {
        }

        /// <summary>
        /// Gets the Value
        /// </summary>
        public T Value { get; }

        public static implicit operator T(AzureFaceValueResult<T> avr) => avr.Value;
        public static explicit operator AzureFaceValueResult<T>(T someObj) => new AzureFaceValueResult<T>(someObj);
    }
}
