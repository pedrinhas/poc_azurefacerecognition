﻿namespace POC_AzureFaceRecognition
{
    using System;

    /// <summary>
    /// Defines the <see cref="AzureFaceResult" />
    /// </summary>
    public class AzureFaceResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AzureFaceResult"/> class.
        /// </summary>
        /// <param name="success">The success<see cref="bool"/></param>
        public AzureFaceResult(bool success)
        {
            Success = success;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureFaceResult"/> class.
        /// </summary>
        /// <param name="ex">The ex<see cref="Exception"/></param>
        public AzureFaceResult(Exception ex) : this(false)
        {
            Exception = ex;
        }

        /// <summary>
        /// Gets a value indicating whether Success
        /// Gets or sets a value indicating whether Success
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Gets the Exception
        /// Gets or sets the Exception
        /// </summary>
        public Exception Exception { get; private set; }

        public static implicit operator bool(AzureFaceResult ar) => ar.Success;
        public static explicit operator AzureFaceResult(bool b) => new AzureFaceResult(b);
    }
}
